import React from 'react';
import {
  StyledCreatedAt,
  StyledLikesCount,
  StyledMainImage,
  StyledMainImageContainer,
  StyledPhotoDescription,
  StyledProfileContainer,
  StyledUserContainer,
  StyledUserImage,
  StyledUserName,
  StyledUserProfileContainer,
} from './StyledProfile';

const Profile = ({ data }) => {
  if (!data) {
      return null;
  };

  return (
    <StyledProfileContainer>
      <StyledMainImageContainer>
        <StyledMainImage alt={data.alt_description} src={data.urls.regular} />
      </StyledMainImageContainer>
      <StyledUserContainer>
        <StyledUserProfileContainer>
          <StyledUserName>
            {data.user.first_name + ' ' + data.user.last_name}
          </StyledUserName>
          <StyledUserImage alt='user' src={data.user.profile_image.large} />
        </StyledUserProfileContainer>
        <StyledPhotoDescription>{data.description}</StyledPhotoDescription>
        <StyledLikesCount>Likes: {data.likes} persones</StyledLikesCount>
        <StyledCreatedAt>Created at: {data.created_at}</StyledCreatedAt>
      </StyledUserContainer>
    </StyledProfileContainer>
  );
};

export default Profile;
