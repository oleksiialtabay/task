import styled from 'styled-components';

export const StyledProfileContainer = styled.div`
    display: flex;
    justify-content: space-between;
`;

export const StyledMainImageContainer = styled.div`
    width: 50%;
`;

export const StyledMainImage = styled.img`
    width: 100%;
`;

export const StyledUserContainer = styled.div`
    position: relative;
    width: 50%;
`;

export const StyledUserProfileContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: flex-end;
`;

export const StyledPhotoDescription = styled.h2`
    text-align: center;
`;

export const StyledUserName = styled.h3`
    font-weight: bold;
`;

export const StyledUserImage = styled.img`
    margin-left: 20px;
    border-radius: 50%;
`;

export const StyledLikesCount = styled.div`
    position: absolute;
    bottom: 50px;
    left: 40px;
    font-weight: bold;
`;

export const StyledCreatedAt = styled.div`
    position: absolute;
    bottom: 20px;
    left: 20px;
`;
