import React, { useEffect, useState } from 'react';
import { getData } from './api/api';
import Profile from './components/Profile';

const App = () => {
  const [data, setData] = useState(null);

  const loadData = () => {
      setInterval(() => {
        getData().then((res) => setData(res.data[0]));
      }, 6000)
  }

  useEffect(() => {
    getData().then((res) => setData(res.data[0]));
    loadData();
  }, []);

  return <Profile data={data} />;
};

export default App;
